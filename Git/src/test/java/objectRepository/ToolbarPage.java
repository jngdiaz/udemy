package objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import ru.yandex.qatools.allure.annotations.Step;
import testcases.Dashboard;

public class ToolbarPage {

    WebDriver driver;
    public ToolbarPage(WebDriver driver) //Constructor to initialize object
    {
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(css="div:nth-child(1) > a > span")
    private WebElement dashboardTab;

    @FindBy(css="div:nth-child(2) > a > span")
    public WebElement messageTab;

    @FindBy(css="div:nth-child(3) > a > span")
    private WebElement contactTab;

    @FindBy(css="div:nth-child(4) > a > span")
    private WebElement buyCreditsTab;

    @FindBy(css="div:nth-child(5) > a > span")
    private WebElement settingsTab;

    @Step("Click Dashboard Tab")
    public Dashboard clickDashboard(){
        dashboardTab.click();
        return PageFactory.initElements(driver, Dashboard.class);
    }


    @Step("Click Message Tab")
    public ToolbarPage clickMessage() {
        messageTab.click();
        return new ToolbarPage(driver);
    }


}



