package objectRepository;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import ru.yandex.qatools.allure.annotations.Step;

import java.util.List;

import static org.testng.Assert.assertFalse;
import static org.testng.Assert.assertTrue;

public class MessagePage {


    static WebDriver driver;
    public MessagePage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }


    @FindBy(css="#messageName")
    private WebElement enterCampaignName;

    @FindBy(css="div.box.m-1.lead-list-box > button")
    private WebElement addLeadListButton;

    @FindBy(css="div.row.mb-4 > div:nth-child(2) > div > input")
    private WebElement searchLeadList;

    @FindBy(css="td:nth-child(2)")
    private WebElement selectLeadList;
    
    @FindBy(css="div.sd.modal-body > div.mt-5.modal-footer > button.btn.main_btn--primary.ml-3.proceedBtn")
    private WebElement nextButton;

    @FindBy(css="div.row.mb-5.mt-2 > div:nth-child(1)")
    private WebElement selectMobileNumber;

    @FindBy(css="div:nth-child(2) > div.mt-5.modal-footer > button.btn.main_btn--primary.ml-3.proceedBtn")
    private  WebElement AddLeadList;

    @FindBy(css="#exampleFormControlTextarea1")
    private WebElement enterCampaignMessage;

    @FindBy(css = ".btn.main-btn.py-2.px-4")
    private WebElement sendNowButton;


    @FindBy(css="div:nth-child(1) > div > a > button")
    private static WebElement conditionsButton;

    @FindBy(css="div.col-md-12.col-lg-2.pr-lg-0.message-side-bar.mb-5 > div > div:nth-child(2) > div > button")
    private  WebElement shortenerButton;

    @FindBy(css="div.input-group.mb-3.api-key > input")
    private  WebElement urlTextbox;

    @FindBy(css="div.input-group.mb-3.api-key > div > button")
    private WebElement shortenURLButton;

    @FindBy(css="button.btn.inverse_btn--primary.revertBtn.mr-3.cancel")
    private WebElement cancelShortenUrl;

    @FindBy(css="button.btn.main_btn--primary.proceedBtn.regenerate")
    private WebElement copyShortURL;

    @FindBy(css="button.btn.main_btn--secondary.mb-1")
    private WebElement buyCreditsButton;

    @FindBy(css="button.btn.main_btn--primary.mb-1")
    private WebElement paymentHistoryButton;

    @FindBy(css="div.col-lg-8.col-md-8.col-sm-12.col-xs-12.welcome-campaign-btn")
    private WebElement sendNewlyAddedButton;

    @FindBy(xpath="//*[@class='btn main-btn dropdown-toggle dropdown-toggle-split']")
    private WebElement campaignScheduleDropdown;

    @FindBy(css="button.btn.main-btn.dropdown-toggle.dropdown-toggle-split")
    private WebElement campaignScheduleOption;

    @FindBy(css="div.form-group.mt-5.mb-5 > small")
    List<WebElement> mcmcregulationtext;

    @FindBy(css="div.screen > div > p")
    private WebElement mobilePreview;

    @FindBy(css="div.css-1uccc91-singleValue")
    private WebElement timezoneDropdown;

    @FindBy(css="welcomeToScheduledPopUp")
    private static WebElement welcomeToScheduledPopUp;

    @FindBy(css="button.emoji-btn.customSave")
    private static WebElement openSaveTemplate;

    @FindBy(css="templateName")
    private static WebElement enterTemplateName;

    @FindBy(css="button.btn.main_btn--primary.proceedBtn.regenerate")
    private static WebElement saveMessageTemplate;

    @Step("Open option to save template")
    public MessagePage openSaveTemplate(){
        try{
            openSaveTemplate.click();
        }
        catch (org.openqa.selenium.StaleElementReferenceException ex)
        {
            openSaveTemplate.click();
        }

        return this;
    }



    @Step("Save template name")
    public MessagePage saveTemplateMessage(){
        saveMessageTemplate.click();
        return this;
    }


    @Step("Check welcome to schedule popup is displayed")
    public static boolean isWelcomeToSchedulePopUpDisplayed(){
        return welcomeToScheduledPopUp.isDisplayed();
    }

    @Step("Click send newly added leads button")
    public MessagePage clickSendNewlyAdded(){
        sendNewlyAddedButton.click();
        return this;
    }

    @Step("Click campaign Schedule dropdown")
    public MessagePage clickCampaignScheduleDropdown(){
        campaignScheduleDropdown.click();
        return this;
    }

    @Step("Click Add lead list button to add recipient")
    public  MessagePage clickAddLeadList(){
        try{
            AddLeadList.click();
        }
        catch (org.openqa.selenium.StaleElementReferenceException ex)
        {
            AddLeadList.click();
        }

        return this;
    }


    @Step("Click Buy Credits")
    public MessagePage clickBuyCredits(){
        buyCreditsButton.click();
        return this;
    }

    @Step("Click Payment History")
    public MessagePage clickPaymentHistory()
    {
        paymentHistoryButton.click();
        return this;
    }


    @Step("Get timezone value")
    public String checkTimezone(){
        String timezone = timezoneDropdown.getText();
        return timezone ;
    }


    @Step("Check MCMC text")
    public boolean checkmcmctext() {

        String timezone = timezoneDropdown.getText();

        if (timezone != "Kuala Lumpur") {
            return mcmcregulationtext.isEmpty();
        } else if (timezone == "Kuala Lumpur") {
            return !mcmcregulationtext.isEmpty();
        }
        return true;
    }


    @Step("Is Conditions Disabled")
    public static boolean isConditionsButtonEnabled(){
        return conditionsButton.isEnabled();
    }

    @Step("Enter Campaign Name")
    public MessagePage enterCampaignName(){

        try{
            enterCampaignName.click();
            enterCampaignName.sendKeys("Automation Campaign");
        }
        catch (org.openqa.selenium.StaleElementReferenceException ex)
        {
            enterCampaignName.click();
            enterCampaignName.sendKeys("Automation Campaign");
        }
        return this;
    }

    @Step("Enter Campaign Message")
    public MessagePage enterCampaignMessage()  {
        enterCampaignMessage.click();
        enterCampaignMessage.sendKeys("CampaignThis is a long campaign");
        return this;
    }

    @Step("Add Lead List Button")
    public MessagePage openLeadList(){
        addLeadListButton.click();
        return  this;
    }

    @Step("Search lead list")
    public MessagePage searchLeadList(){
        searchLeadList.sendKeys("Automation Test");
        return this;
    }

    @Step("Select lead list")
    public MessagePage selectLeadList(){
        selectLeadList.click();
        return this;
    }

    @Step("Next to select mobile number")
    public MessagePage nextButton(){
        JavascriptExecutor executor = (JavascriptExecutor) driver;
        executor.executeScript("arguments[0].scrollIntoView(true);", nextButton);
        nextButton.click();
        return this;
    }

    @Step("Select mobile number column")
    public MessagePage selectMobileNumber(){
        selectMobileNumber.click();

        return this;
    }

    @Step("Click Add Lead List Button")
    public MessagePage addLeadListButton(){
        addLeadListButton.click();
        return this;
    }

    @Step("Send now campaign")
    public MessagePage sendNowButton(){
         sendNowButton.click();
         return this;
    }

    @Step("Shorten URL to add in campaign message")
    public MessagePage addShortenURL(){ //smallLetters
        shortenerButton.click();
        urlTextbox.sendKeys("https://unsplash.com/s/photos/cactus");
        shortenURLButton.click();
        String shortenURL= urlTextbox.getAttribute("value");
        copyShortURL.click();
        cancelShortenUrl.click();
        enterCampaignMessage.click();
        enterCampaignMessage.sendKeys(shortenURL);
        return this;
    }



    @Step("Add a lead list")
    public MessagePage addLeadList(){
        MessagePage messagePage = new MessagePage(driver);
        messagePage
                .openLeadList()
                .searchLeadList()
                .selectLeadList()
                .nextButton()
                .selectMobileNumber()
                .clickAddLeadList();
        return this;
    }


}
