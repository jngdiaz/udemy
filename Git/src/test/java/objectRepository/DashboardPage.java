package objectRepository;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
import org.testng.Assert;
import ru.yandex.qatools.allure.annotations.Step;

public class DashboardPage {

    WebDriver driver;
    public DashboardPage(WebDriver driver){
        this.driver=driver;
        PageFactory.initElements(driver,this);
    }

    @FindBy(css="tr:nth-child(1) > td:nth-child(2) > div > span")
    private WebElement firstDraft;

    @FindBy(css="div.col-xl-9.col-lg-8.col-md-12.mb-3  div  div  div.row  div:nth-child(1) > h3")
    private WebElement broadcastingReport;

    @FindBy(css ="div.row.py-4.service-cards > div:nth-child(1) > div > div > a")
    private WebElement quickMessageButton;

    @FindBy(css="div:nth-child(2) > div > div > a")
    private WebElement smsCampaignButton;

    @FindBy(css = "div:nth-child(3) > div > div > a")
    private WebElement smsAnalyticsButton;

    @FindBy(css="div:nth-child(4) > div > div > a")
    private WebElement leadsButton;

    @FindBy(css="div:nth-child(1) > h2")
    private WebElement campaignOverview;

    @FindBy(css="li:nth-child(2) button")
    private WebElement draftTab;

    @FindBy(css="div:nth-child(2) > ul > li:nth-child(1) > button")
    private WebElement runningTab;

    @FindBy(css="div:nth-child(2) > ul > li:nth-child(3) > button")
    private WebElement stoppedTab;

    @FindBy(css="div:nth-child(2) > ul > li:nth-child(4) > button")
    private WebElement insufficientTab;

    @FindBy(css="div:nth-child(2) > ul > li:nth-child(5) > button")
    private WebElement finishedTab;

    @FindBy(css="th:nth-child(1)")
    private  WebElement campaignName;

    @FindBy(css="th:nth-child(2)")
    private  WebElement leadList;

    @FindBy(css="th:nth-child(3)")
    private  WebElement type;

    @FindBy(css="th:nth-child(4)")
    private  WebElement createdDate;

    @FindBy(css="th:nth-child(5)")
    private  WebElement startDate;

    @FindBy(css="th:nth-child(6)")
    private  WebElement endDate;

    @FindBy(css="th:nth-child(7)")
    private  WebElement timezone;

    @FindBy(css="th:nth-child(8)")
    private  WebElement scheduleTime;

    @FindBy(css="th:nth-child(9)")
    private  WebElement status;

    @FindBy(css="div.row.campaign-status-nav > div:nth-child(1) > a")
    private  WebElement viewMore;

    @Step("Check Broadcasting Report Text")
    public String getBroadcastingReportText(){
        return broadcastingReport.getText();
    }

    @Step("Click the first record in draft")
    public DashboardPage clickFirstRecord(){
        firstDraft.click();
        return this;
    }


    @Step("Check SMS Campaign Text")
    public String getSMSCampaignText(){
        return smsCampaignButton.getText();
    }


    @Step("Check SMS Analytics Text")
    public String getSMSAnalyticsText(){
        return  smsAnalyticsButton.getText();
    }


    @Step("Check Leads Text")
    public String getLeadsText(){
        return  leadsButton.getText();
    }

    @Step("Check Campaign Overview Text")
    public boolean isCampaignOverviewTextDisplayed(){
        campaignOverview.isDisplayed();
        return true;
    }

    @Step("Check Campaign Message Text")
    public String getCampaignText(){
        return campaignOverview.getText();
    }

    @Step("Click Draft Tab")
    public DashboardPage clickDraftTab(){
        draftTab.click();
        return this;
    }

    @Step("Click Running Tab")
    public DashboardPage clickRunningTab(){
        runningTab.click();
        return this;
    }

    @Step("Click Stopped Tab")
    public DashboardPage clickStoppedTab(){
        stoppedTab.click();
        return this;
    }


    @Step("Click Insufficient Credit Tab")
    public DashboardPage clickInsufficientTab(){
        insufficientTab.click();
        return this;
    }


    @Step("Click Finished Tab")
    public DashboardPage clickFinishedTab(){
        finishedTab.click();
        return this;
    }

    @Step("Check Campaign Name Column")
    public String getCampaignNametext(){
        return campaignName.getText();

    }

    @Step("Check Lead list Column")
    public String getLeadText(){
        return leadList.getText();

    }

    @Step("Check Type Column")
    public String getTypeText(){
        return type.getText();

    }


    @Step("Check Created Date Column")
    public String getCreatedDateText(){
        return createdDate.getText();

    }

    @Step("Check Start Date Column")
    public String getStartDateText(){
        return startDate.getText();

    }

    @Step("Check End Date Column")
    public String getEndDateText(){
        return endDate.getText();

    }

    @Step("Check Timezone Column")
    public String getTimezoneText(){
        return timezone.getText();

    }

    @Step("Check Schedule Time Column")
    public String getScheduleTimeText(){
        return scheduleTime.getText();

    }

    @Step("Check Status Column")
    public String getStatusText(){
        return status.getText();

    }

    @Step("Redirect to view more URL")
    public DashboardPage clickViewMore(){
        viewMore.click();
        return this;
    }

    @Step("Check URL")
    public DashboardPage checkURL(){
        String URL =driver.getCurrentUrl();
        Assert.assertEquals(URL,"http://192.168.28.47/dev/dashboard/campaigns-list");
        return this;
    }

    @Step("Check Columns")
    public DashboardPage checkColumnNames(){

        String CampaignNameText=getCampaignNametext();
        Assert.assertEquals(CampaignNameText,"Campaign name");

        String LeadListText=getLeadText();
        Assert.assertEquals(LeadListText,"Lead list");

        String TypeText=getTypeText();
        Assert.assertEquals(TypeText,"Type");

        String CreatedDateText=getCreatedDateText();
        Assert.assertEquals(CreatedDateText,"Created date");


        String StartDateText=getStartDateText();
        Assert.assertEquals(StartDateText,"Start date");

        String EndDateText=getEndDateText();
        Assert.assertEquals(EndDateText,"End date");

        String TimezoneText=getTimezoneText();
        Assert.assertEquals(TimezoneText,"Timezone");

        String ScheduledTime=getScheduleTimeText();
        Assert.assertEquals(ScheduledTime,"Scheduled time");

        String StatusText=getStatusText();
        Assert.assertEquals(StatusText,"Status");

        return PageFactory.initElements(driver, DashboardPage.class);
    }

    @Step("Check Broadcasting report text")
    public DashboardPage checkBroadcastingReportText() {

        //BroadcastingReport
        String BroadcastingReportText = getBroadcastingReportText();
        Assert.assertEquals(BroadcastingReportText, "Broadcasting Report");

        return PageFactory.initElements(driver, DashboardPage.class);
    }

    @Step("Dashboard redirect to Quick Message")
    public DashboardPage redirectquickmessage() {
        quickMessageButton.click();
        return this;
    }

    @Step("Dashboard redirect to SMS Campaign")
    public DashboardPage redirectsmscampaign() {
        smsCampaignButton.click();
        return this;
    }

    @Step("Dashboard redirect to SMS Analytics")
    public DashboardPage redirectsmsanalytics() {
        smsAnalyticsButton.click();
        return this;
    }

    @Step("Dashboard redirect to Leads")
    public DashboardPage redirectleads() {
        leadsButton.click();
        return this;
    }

    @Step("Check Campaign overview text")
    public DashboardPage checkCampaignOverviewText() {

        //CampaignOverview
        String CampaignOverviewText=getCampaignText();
        Assert.assertEquals(CampaignOverviewText,"Campaign Overview");

        return PageFactory.initElements(driver, DashboardPage.class);
    }


}






