package testcases;
import Resources.Base;
import objectRepository.LoginPage;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.IOException;

public class Login extends Base {

    @TestCaseId("C43")
    @Test
    public void loginuser() throws IOException {
        driver=initializer(); //return driver data from Base.java
        LoginPage li=new LoginPage(driver);
        li.login();
    }
}
