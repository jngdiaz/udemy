package testcases;

import Resources.Base;
import objectRepository.DashboardPage;
import objectRepository.LoginPage;
import objectRepository.MessagePage;
import objectRepository.ToolbarPage;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.IOException;

public class Message extends Base {

    @TestCaseId("C1598 Conditions is disabled by default")
    @Test
    public void conditionsisdisabled() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickMessage();
        MessagePage messagePage = new MessagePage(driver);
        boolean buttonStatus = messagePage.isConditionsButtonEnabled();
        Assert.assertFalse(buttonStatus);
    }

    @Test
    public void welcometoscheduledpopup() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickMessage();
        MessagePage messagePage = new MessagePage(driver);
        messagePage.clickSendNewlyAdded()
                .clickCampaignScheduleDropdown();
        //.isWelcomeToSchedulePopUpDisplayed();
    }

    @TestCaseId("C1987 MCMC text is displayed")
    @Test
    public void checkmcmctext() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickMessage();
        MessagePage messagePage = new MessagePage(driver);
        messagePage.checkmcmctext();
    }

    @Test
    public void checkbuycreditspage() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickMessage();
        MessagePage messagePage = new MessagePage(driver);
        messagePage.clickBuyCredits();
        String BuyCreditsPage = driver.getCurrentUrl();
        Assert.assertEquals(BuyCreditsPage, "http://192.168.28.47/dev/dashboard/payment/buy-credits" );
    }

    @Test
    public void savetemplate() throws IOException {
        driver = initializer();
        LoginPage li = new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickMessage();
        MessagePage messagePage = new MessagePage(driver);
        messagePage.enterCampaignMessage()
                .openSaveTemplate();

    }



    @Test
    public void checkpaymenthistorypage() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickMessage();
        MessagePage messagePage = new MessagePage(driver);
        messagePage.clickPaymentHistory();
        String BuyPaymentHitory = driver.getCurrentUrl();
        Assert.assertEquals(BuyPaymentHitory, "http://192.168.28.47/dev/dashboard/account/billing-and-payments" );
    }

    @Test
    public void createsendnowcampaign() throws IOException //smallletters
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickMessage();
        MessagePage messagePage = new MessagePage(driver);
        messagePage
                .openLeadList()
                .searchLeadList()
                .selectLeadList()
                .nextButton()
                .selectMobileNumber()
                .clickAddLeadList()
                .enterCampaignName()
                .addShortenURL()
                .enterCampaignMessage()
                .sendNowButton();
    }

    @Test
    public void sendnowbuttonisclickable() throws IOException {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickMessage();
        MessagePage messagePage = new MessagePage(driver);
        messagePage.addLeadList();
       /* boolean sendNow = messagePage.sendNowButton();
        Assert.assertTrue(sendNow);*/






    }

}
