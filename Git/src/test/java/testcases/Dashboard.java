package testcases;


import Resources.Base;
import objectRepository.DashboardPage;
import objectRepository.LoginPage;
import objectRepository.ToolbarPage;
import org.testng.Assert;
import org.testng.annotations.Test;
import ru.yandex.qatools.allure.annotations.TestCaseId;

import java.io.IOException;

public class Dashboard extends Base {


    @TestCaseId("C61 UI : Dashboard redirect to Quick Message")
    @Test
    public void redirectquickmessage() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickDashboard();
        DashboardPage dashboardPage= new DashboardPage(driver);
        dashboardPage.redirectquickmessage();
        String qmurl= driver.getCurrentUrl();
        Assert.assertEquals(qmurl, "http://192.168.28.47/dev/dashboard/quick-sms" );
        driver.close();
    }

    @TestCaseId("C62 UI: 'Dashboard redirect to SMS Campaign'")
    @Test
    public void redirectSMSCampaign() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickDashboard();
        DashboardPage dashboardPage= new DashboardPage(driver);
        dashboardPage.redirectsmscampaign();
        String smsCampaignUrl = driver.getCurrentUrl();
        Assert.assertEquals(smsCampaignUrl, "http://192.168.28.47/dev/dashboard/campaign/create-new" );
        driver.close();

    }

    @TestCaseId("C64 UI : Dashboard redirect to Leads")
    @Test
    public void redirectleads() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickDashboard();
        DashboardPage dashboardPage= new DashboardPage(driver);
        dashboardPage.redirectleads();
        String leadsUrl=driver.getCurrentUrl();
        Assert.assertEquals(leadsUrl,"http://192.168.28.47/dev/dashboard/contact");
        //driver.close();
    }

    @TestCaseId("C63 UI : 'Dashboard to SMS Analytics")
    @Test
    public void redirectSMSAnalytics() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickDashboard();
        DashboardPage dashboardPage= new DashboardPage(driver);
        dashboardPage.redirectsmsanalytics();
        String smsAnalyticsUrl=driver.getCurrentUrl();
        Assert.assertEquals(smsAnalyticsUrl,"http://192.168.28.47/dev/dashboard/sms-analytics");
        driver.close();
    }



    @TestCaseId("C81 Campaign overview VIEW MORE to see all campaigns by status")
    @Test
    public void clickViewMoreCampaigns() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickDashboard();
        DashboardPage dashboardPage= new DashboardPage(driver);
        dashboardPage.clickRunningTab()
                .clickViewMore()
                .checkURL();
    }

    @TestCaseId("Campaign Overview Tabs")
    @Test
    public void checkCampaignOverviewText() throws IOException
    {
        driver=initializer();
        LoginPage li=new LoginPage(driver);
        ToolbarPage toolbarPage = li.login();
        toolbarPage.clickDashboard();
        DashboardPage dashboardPage= new DashboardPage(driver);
        dashboardPage.checkCampaignOverviewText()
                .clickRunningTab()
                .checkColumnNames()
                .clickDraftTab()
                .checkColumnNames()
                .clickInsufficientTab()
                .checkColumnNames()
                .clickStoppedTab()
                .checkColumnNames()
                .clickFinishedTab()
                .checkColumnNames();
    }


}
