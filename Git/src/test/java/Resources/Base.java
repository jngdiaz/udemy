package Resources;

import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;

import java.io.FileInputStream;
import java.io.IOException;
import java.util.Properties;
import java.util.concurrent.TimeUnit;

public class Base {

    public WebDriver driver;
    public Properties prop;


    public WebDriver initializer() throws IOException{

        prop=new Properties();
        FileInputStream fis=new FileInputStream("C:\\Users\\erika\\A2P\\src\\test\\java\\A2P\\dataFile.properties");
        prop.load(fis);

        String browserName=prop.getProperty("browser");

        if(browserName.equals("chrome"))
        {
            System.setProperty("webdriver.chrome.driver","C:\\Selenium\\chromedriver.exe");
            driver=new ChromeDriver();
        }

        else if (browserName.equals("firefox"))
        {
            driver=new FirefoxDriver();
        }

        else if(browserName.equals("IE"))
        {
            driver=new FirefoxDriver();
        }
        driver.manage().window().maximize();

        driver.manage().timeouts().implicitlyWait(5, TimeUnit.SECONDS);

        driver.get("http://192.168.28.47/dev/");

        return driver;

    }


}
